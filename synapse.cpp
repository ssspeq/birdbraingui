#include "synapse.h"

synapse::synapse()
{
    activated = false;
    time_to_be_active = 10;
}
synapse::~synapse()
{
}
void synapse::read(const QJsonObject &json)
{
    to = json["to"].toDouble();
    from = json["fr"].toDouble();
    weight = json["wt"].toDouble();
    type = json["te"].toInt();
    usage_level = json["ul"].toInt();
    time_to_be_active = json["tme"].toInt();
}
void synapse::write(QJsonObject &json) const
{
    json["to"] = to;
    json["fr"] = from;
    json["wt"] = weight;
    json["te"] = type;
    json["ul"] = usage_level;
    json["tme"] = time_to_be_active;
}

void synapse::activate()
{
    if(time_activated == 0 && !activated)
    {
        activated = true;
        time_activated = time_to_be_active;
    }
}

void synapse::inactivate()
{
    activated = false;
    time_activated = 0;
}

void synapse::reset()
{
    inactivate();
}

void synapse::update()
{
    if(time_activated > 0)
    {
        time_activated--;
    }
    if(time_activated == 0)
        inactivate();
}
