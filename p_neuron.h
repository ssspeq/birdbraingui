#ifndef P_NEURON_H
#define P_NEURON_H
#include "neuron.h"
class p_neuron : public neuron
{
public:
    void fire();
    p_neuron();
    ~p_neuron();
};

#endif // P_NEURON_H
