#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    myDefaultWidget = new defaultWidget(this);
    setCentralWidget(myDefaultWidget);
    createActions();
    createMenus();
    something_is_loaded = false;
}

MainWindow::~MainWindow()
{
}

void MainWindow::createActions()
{
    newAct = new QAction(tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new net"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newNet()));

    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing net"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(tr("&Save..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save an existing net"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("&Save as..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save an existing net"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    aboutAct = new QAction(tr("&Help..."), this);
    aboutAct->setStatusTip(tr("Help"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    testsAct = new QAction(tr("&Tests..."), this);
    testsAct->setStatusTip(tr("Test models"));
    connect(testsAct, SIGNAL(triggered()), this, SLOT(tests()));

}

void MainWindow::createMenus()
{
    ui->fileMenu->addAction(newAct);
    ui->fileMenu->addAction(openAct);
    ui->fileMenu->addAction(saveAct);
    ui->fileMenu->addAction(saveAsAct);
    ui->helpMenu->addAction(aboutAct);
    ui->menuFunction_tests->addAction(testsAct);
}

void MainWindow::tests()
{
    myDefaultWidget->close();
    functionTesterWidget *myfunctionTesterWidget = new functionTesterWidget(this);
    setCentralWidget(myfunctionTesterWidget);
}

void MainWindow::newNet()
{
    if(something_is_loaded)
    {
        switch(askUserOKUnloadNet())
        {
        case QMessageBox::Save:
            emit saveNet();
            emit close();
            getInputAndMakeNewNet();
            break;
        case QMessageBox::Discard:
            emit close();
            getInputAndMakeNewNet();
            break;
        }
    }
    else
    {
        getInputAndMakeNewNet();
    }
}

int MainWindow::askUserOKUnloadNet()
{
    int reply = QMessageBox::critical(this, "Attention", "Do you want to save this net?",
                                  QMessageBox::Save | QMessageBox::Discard
                                                     | QMessageBox::Cancel);
    return reply;
}

void MainWindow::getInputAndMakeNewNet()
{
    DialogNetCreate *net_create = new DialogNetCreate(this);
    if(net_create->exec())
    {
        qDebug() << "ok";
        neuralNet *new_net = net_create->getLoadedNet();
        delete net_create;
        something_is_loaded = true;
        SRANN *my_net_view = new SRANN(this, &new_net);
        connect( this, SIGNAL(saveNet()), my_net_view, SLOT(onNetSave()));
        connect( this, SIGNAL(close()), my_net_view, SLOT(onClose()));
        connect( my_net_view, SIGNAL(netIsSaved()), this, SLOT(on_netIsSaved()));
        this->setCentralWidget(my_net_view);
    }
    else
    {
        something_is_loaded = false;
    }
}

void MainWindow::open()
{
    if(something_is_loaded)
    {
        switch(askUserOKUnloadNet())
        {
        case QMessageBox::Save:
            emit saveNet();
            emit close();
            showFileOpenDialog();
            break;
        case QMessageBox::Discard:
            emit close();
            showFileOpenDialog();
            break;
        }
    }
    else
    {
        emit close();
        showFileOpenDialog();
    }

}

void MainWindow::showFileOpenDialog()
{
    QFileDialog dialog(this);
    dialog.setNameFilter(tr("Neural nets (*.net)"));
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setFileMode(QFileDialog::ExistingFile);
    QStringList fileNames;
    if (dialog.exec())
    {
        fileNames = dialog.selectedFiles();
        loadFile(fileNames[0]);
    }
}

bool MainWindow::save()
{
    emit saveNet();
    ui->statusBar->showMessage(tr("File saved"), 2000);
    return true;
}

void MainWindow::saveAs()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList files;
    if (dialog.exec())
    {
        files = dialog.selectedFiles();
        saveFile();
    }
}
void MainWindow::about()
{
   QMessageBox::about(this, tr("About Birdbrain"),
            tr("Nothing yet"));
}


bool MainWindow::loadFile(const QString &fileName)
{
    neuralNet *current_net = new neuralNet(0);
    current_net->loadNet(current_net->Json, fileName);
    something_is_loaded = true;
    SRANN *my_net_view = new SRANN(this, &current_net);
    connect( this, SIGNAL(saveNet()), my_net_view, SLOT(onNetSave()));
    connect( this, SIGNAL(close()), my_net_view, SLOT(onClose()));
    connect( my_net_view, SIGNAL(netIsSaved()), this, SLOT(on_netIsSaved()));
    this->setCentralWidget(my_net_view);
    return true;
}

bool MainWindow::saveFile()
{
    //emit saveNet();
    return true;
}

void MainWindow::on_netIsSaved()
{
    ui->statusBar->showMessage(tr("File saved"), 2000);
}
