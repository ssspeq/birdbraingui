#ifndef RANDOM_H
#define RANDOM_H
#include <stdlib.h>

int RandLim(int limit);
float RandFloatMinusOneToOne();
float RandFloatNegativeWeight();
float RandFloatPositiveWeight();
float RandFloat();
#endif // RANDOM_H
