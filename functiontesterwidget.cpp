#include "functiontesterwidget.h"
#include "ui_functiontesterwidget.h"

functionTesterWidget::functionTesterWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::functionTesterWidget)
{
    ui->setupUi(this);
    ui->neuronModelComboBox->addItem("Realistic");
    QLinearGradient gradient(QPointF(50, -20), QPointF(80, 20));
    gradient.setColorAt(0.0, Qt::white);
    gradient.setColorAt(1.0, QColor(0xa6, 0xce, 0x39));
    scene = new QGraphicsScene;
    ui->gW->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
    ui->gW->setRenderHint(QPainter::Antialiasing);
    QBrush background = QBrush(QColor(10, 20, 40));
    scene->setBackgroundBrush(background);
    ui->gW->setScene(scene);
    neuron_rad = 40;
    synapse_rad = 25;
    dendrite_rad = 25;
    my_pen = new QPen(Qt::black);
}

void functionTesterWidget::setupScene()
{
    //neuron
    neuron_core = scene->addEllipse(0, 200, neuron_rad*2.0, neuron_rad*2.0,
                *my_pen, QColor(100, 200, 40));
    //dendrites
    int dx = -150;
    int dy = 80;
    QHash<qint32, synapse *>::iterator i;
    for(i=incoming_synapses->begin();i!=incoming_synapses->end();i++)
    {
        dy+=55;
        scene->addEllipse(dx, dy, dendrite_rad*2.0, dendrite_rad*2.0,
                        *my_pen, QColor(150, 150, 150));
    }

    //synapses
    int sx = 150;
    int sy = 160;
    QLinkedList<synapse*>::iterator it;
    for(it=my_neuron->synapse_list->begin();it!=my_neuron->synapse_list->end();it++)
    {
        sy+=55;
        scene->addEllipse(sx, sy, synapse_rad*2.0, synapse_rad*2.0,
                        *my_pen, QColor(200, 200, 200));
    }
    //Connections

    //scene->addLine(0, 240, -150, 100,
    //                *my_pen);

    ui->gW->setScene(scene);


}

void functionTesterWidget::testAnimation()
{
}

functionTesterWidget::~functionTesterWidget()
{
    delete ui;
}

void functionTesterWidget::on_buttonLoad_clicked()
{
    my_neuron = new neuron;
    incoming_synapses = new QHash<qint32, synapse*>;
    qint32 id = 0;

    inhibitory_synapse *newisynapse = new inhibitory_synapse;
    my_neuron->synapse_list->append(newisynapse);
    //Generate some incoming synapses for neuron
    for(int i = 0; i<2;i++)
    {
        excitatory_synapse *newesynapse = new excitatory_synapse;
        incoming_synapses->insert(id, newesynapse);
        ++id;
    }

    for(int i = 0; i<2;i++)
    {
        inhibitory_synapse *newisynapse = new inhibitory_synapse;
        incoming_synapses->insert(id, newisynapse);
        ++id;
    }
    //Now add them to the neurons dendrite list so it can check them for input
    QHash<qint32, synapse *>::iterator it;
    for(it=incoming_synapses->begin();it!=incoming_synapses->end();it++)
    {
        dendrite *new_dendrite = new dendrite;
        new_dendrite->my_synapse = (*it);
        new_dendrite->weight = RandFloatMinusOneToOne();
        my_neuron->dendrite_list->append(new_dendrite);
    }
    setupScene();
}
