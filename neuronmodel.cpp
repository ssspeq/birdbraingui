#include "neuronmodel.h"

neuronmodel::neuronmodel(QObject *parent, neuralNet **net) :
    QAbstractTableModel(parent)
{
    my_net = *net;
}
int neuronmodel::rowCount(const QModelIndex & /*parent*/) const
 {
    return my_net->node_hash->count();
 }

 int neuronmodel::columnCount(const QModelIndex & /*parent*/) const
 {
     return 9;
 }

 QVariant neuronmodel::data(const QModelIndex &index, int role) const
 {

     int row = index.row();
     int col = index.column();

     // generate a log message when this method gets called
//     qDebug() << QString("row %1, col%2, role %3")
//                 .arg(row).arg(col).arg(role);
     neuron *current_neuron = my_net->node_hash->find(row).value();
     switch(role)
     {
     case Qt::DisplayRole:
         switch(col)
         {
         case 0:
             return current_neuron->id;
         case 1:
             return current_neuron->charge;
         case 2:
             return current_neuron->neuron_type;
         case 3:
             return current_neuron->charge_after_fire;
         case 4:
             return current_neuron->charge_gain;
         case 5:
             return current_neuron->charge_leak;
         case 6:
             return current_neuron->charge_threshold;
         case 7:
             return current_neuron->resting_potential;
         case 8:
             return current_neuron->activation_function;
         }

     }
     return QVariant();
}
 QVariant neuronmodel::headerData(int section, Qt::Orientation orientation, int role) const
  {
      if (role == Qt::DisplayRole)
      {
          if (orientation == Qt::Horizontal) {
              switch (section)
              {
              case 0:
                  return QString("Id");
              case 1:
                  return QString("Charge");
              case 2:
                  return QString("Neuron_type");
              case 3:
                  return QString("Charge after fire");
              case 4:
                  return QString("Charge gain");
              case 5:
                  return QString("Charge leak");
              case 6:
                  return QString("Charge threshold");
              case 7:
                  return QString("Resting potential");
              case 8:
                  return QString("Activation function");
              }
          }
      }
      return QVariant();
  }

 Qt::ItemFlags neuronmodel::flags(const QModelIndex & /*index*/) const
 {
     return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
 }

// bool neuronmodel::setData(const QModelIndex & index, const QVariant & value, int role)
//  {
//      if (role == Qt::EditRole)
//      {
//          //save value from editor to member m_gridData
//          m_gridData[index.row()][index.column()] = value.toString();
//          //for presentation purposes only: build and emit a joined string
//          QString result;
//          for(int row= 0; row < ROWS; row++)
//          {
//              for(int col= 0; col < COLS; col++)
//              {
//                  result += m_gridData[row][col] + " ";
//              }
//          }
//          //emit editCompleted( result );
//      }
//      return true;
//  }

void neuronmodel::Changed()
{
    emit dataChanged(createIndex(0,0),createIndex(0,9));
}

