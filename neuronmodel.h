#ifndef NEURONMODEL_H
#define NEURONMODEL_H
#include <QDebug>
#include "neuralnet.h"
#include <QAbstractTableModel>

const int COLS= 8;
const int ROWS= 2;

class neuronmodel : public QAbstractTableModel
{
    Q_OBJECT
public:
    neuronmodel(QObject *parent, neuralNet **net);
    neuralNet *my_net;
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    //bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    Qt::ItemFlags flags(const QModelIndex & index) const ;
    //QString m_gridData[ROWS][COLS];
    void Changed();
signals:
    void editCompleted(const QString &);
public slots:

};

#endif // NEURONMODEL_H
