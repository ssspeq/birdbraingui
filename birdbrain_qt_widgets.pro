#-------------------------------------------------
#
# Project created by QtCreator 2015-02-05T21:53:01
#
#-------------------------------------------------

QT       += core gui
QT += opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = birdbrain_qt_widgets
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    calculations.cpp \
    dendrite.cpp \
    device.cpp \
    e_neuron.cpp \
    electrical_synapse.cpp \
    excitatory_synapse.cpp \
    i_neuron.cpp \
    inhibitory_synapse.cpp \
    neuron.cpp \
    o_neuron.cpp \
    p_neuron.cpp \
    random.cpp \
    s_neuron.cpp \
    dialognetcreate.cpp \
    neuronmodel.cpp \
    synapse.cpp \
    neuralnet.cpp \
    srannloadedwindow.cpp \
    defaultwidget.cpp \
    functiontesterwidget.cpp

HEADERS  += mainwindow.h \
    calculations.h \
    constants.h \
    dendrite.h \
    device.h \
    e_neuron.h \
    electrical_synapse.h \
    excitatory_synapse.h \
    i_neuron.h \
    inhibitory_synapse.h \
    neuron.h \
    o_neuron.h \
    p_neuron.h \
    random.h \
    s_neuron.h \
    dialognetcreate.h \
    neuronmodel.h \
    synapse.h \
    neuralnet.h \
    netreturnstruct.h \
    srannloadedwindow.h \
    defaultwidget.h \
    buildnumber.h \
    functiontesterwidget.h

FORMS    += mainwindow.ui \
    dialognetcreate.ui \
    srannloadedwindow.ui \
    defaultwidget.ui \
    functiontesterwidget.ui



LIBS += -lglut -lGL -lGLU -lGLEW

OTHER_FILES +=

RESOURCES += \
    res.qrc
DEFINES+="BUILDNUMBER=12345"
