#include "neuron.h"
neuron::neuron()
{
    synapse_list = new QLinkedList<synapse *>();
    dendrite_list = new QLinkedList<dendrite *>();
    x = RandFloatMinusOneToOne();
    y = RandFloatMinusOneToOne();
    z = RandFloatMinusOneToOne();
    activation_function = ACTIVATION_FUNCTION_LEVEL;
    charge_threshold = 0.7;
    charge_after_fire = -0.5;
    charge_gain = 0;
    charge_leak = -0.1;
    charge_threshold_self_fire = 0;
    resting_potential = 0;
    charge = 0;
}
neuron::~neuron()
{

    if(synapse_list->isEmpty() != true)
    {
        QLinkedList<synapse *>::iterator i;
        for (i = synapse_list->begin(); i != synapse_list->end(); i++)
        {
            delete(*i);
        }
        synapse_list->clear();

    }
    delete synapse_list;
    //Now dendrites THIS IS PROBABLY NOT NEEDED, JUST DELETE LIST
    if(dendrite_list->isEmpty() != true)
    {
        QLinkedList<dendrite *>::iterator i;
        for (i = dendrite_list->begin(); i != dendrite_list->end(); i++)
        {
            delete(*i);
        }
        dendrite_list->clear();

    }
    delete dendrite_list;
}

void neuron::read(const QJsonObject &json)
{
    id = json["id"].toDouble();
    activation_function = json["af"].toInt();
    x =json["x"].toDouble();
    y =json["y"].toDouble();
    z =json["z"].toDouble();
    neuron_type = json["nt"].toInt();
    charge_threshold = json["ct"].toDouble();
    charge_gain = json["cg"].toDouble();
    charge_leak = json["cl"].toDouble();
    resting_potential = json["rp"].toDouble();
    charge_after_fire = json["caf"].toDouble();
    synapse_list->clear();

        QJsonArray synapse_array = json["syns"].toArray();
        for (int synapse_index = 0; synapse_index < synapse_array.size();
             ++synapse_index)
        {
            QJsonObject synapse_object = synapse_array[synapse_index].toObject();
            synapse *new_synapse = NULL;
            switch(synapse_object["te"].toInt())
            {
            case ELECTRICAL:
                new_synapse = new electrical_synapse;
                break;
            case CHEMICAL_EXC:
                new_synapse = new excitatory_synapse;
                break;
            case CHEMICAL_INHIB:
                new_synapse = new inhibitory_synapse;
                break;
            }
            new_synapse->read(synapse_object);
            synapse_list->append(new_synapse);
        }
        //now dendrites
        QJsonArray dendrite_array = json["dens"].toArray();
        for (int dendrite_index = 0; dendrite_index < dendrite_array.size();
             ++dendrite_index)
        {
            QJsonObject dendrite_object = dendrite_array[dendrite_index].toObject();
            dendrite *new_dendrite = new dendrite;
            new_dendrite->read(dendrite_object);
            dendrite_list->append(new_dendrite);
        }
}
void neuron::write(QJsonObject &json) const
{
    json["id"] = id;
    json["af"] = activation_function;
    json["x"] = x;
    json["y"] = y;
    json["z"] = z;
    json["nt"] = neuron_type;
    json["ct"] = charge_threshold;
    json["cg"] = charge_gain;
    json["cl"] = charge_leak;
    json["rp"] = resting_potential;
    json["caf"] = charge_after_fire;

    QJsonArray synapse_array;
    QLinkedList<synapse *>::iterator i;
    for (i = synapse_list->begin(); i != synapse_list->end(); ++i)
    {
        QJsonObject json_synapse;
        (*i)->write(json_synapse);
        synapse_array.append(json_synapse);
    }
    json["syns"] = synapse_array;
    //Now dendrites


    QJsonArray dendrite_array;
    QLinkedList<dendrite *>::iterator it;
    for (it = dendrite_list->begin(); it != dendrite_list->end(); ++it)
    {
        QJsonObject json_dendrite;
        (*it)->write(json_dendrite);
        dendrite_array.append(json_dendrite);
    }
    json["dens"] = dendrite_array;
}

bool neuron::activationFunctionLevel()
{
    if(charge > charge_threshold)
    {
        return true;
    }
    else
    {
        return false;
    }
}
void neuron::fire()
{
    //trigger all synapses cuz we fired
    QLinkedList<synapse *>::iterator i;
    for (i = synapse_list->begin(); i != synapse_list->end(); ++i)
    {
        (*i)->activate();
    }
    charge = charge_after_fire;
}
int neuron::doCompleteTimeStep()
{
    checkDendrites();
    if(activationFunctionLevel())
    {
        fire();
    }
    else
    {
        updateSynapses();
    }
    return 0;
}

void neuron::updateSynapses()
{
    QLinkedList<synapse *>::iterator i;
    for (i = synapse_list->begin(); i != synapse_list->end(); ++i)
    {
        (*i)->update();
    }
}

void neuron::checkDendrites()
{
    //Go through dendrites so we can update our status so we know if we should
    //fire

    QLinkedList<dendrite *>::iterator i;
    for (i = dendrite_list->begin(); i != dendrite_list->end(); ++i)
    {
        //What kind of synapse is messing with us?
        if((*i)->my_synapse->activated)
        {
            switch((*i)->my_synapse->type)
            {
            case CHEMICAL_EXC:
                charge += CHEMICAL_EXC_EFFECT;
            case CHEMICAL_INHIB:
                charge -= CHEMICAL_INHIB_EFFECT;
            }
        }
    }
}

void neuron::reset()
{
    charge = 0;
    QLinkedList<synapse *>::iterator i;
    for (i = synapse_list->begin(); i != synapse_list->end(); ++i)
    {
        (*i)->reset();

    }
}
