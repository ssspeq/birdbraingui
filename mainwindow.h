#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "dialognetcreate.h"
#include <QHash>
#include <QStringList>
#include <QMessageBox>
#include <QShortcut>
#include <QThread>
#include "neuralnet.h"
#include "netreturnstruct.h"
#include "neuronmodel.h"
#include "srannloadedwindow.h"
#include "defaultwidget.h"
#include "functiontesterwidget.h"

Q_DECLARE_METATYPE( net_return_data );

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void newNet();
    void open();
    bool save();
    void saveAs();
    void about();
    void getInputAndMakeNewNet();
    int askUserOKUnloadNet();
    void on_netIsSaved();
    void tests();

private:
    Ui::MainWindow *ui;
    QMenu *fileMenu;
    QMenu *helpMenu;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *aboutAct;
    QAction *runAct;
    QAction *testsAct;
    defaultWidget *myDefaultWidget;
    functionTesterWidget myfunctionTesterWidget;

    bool something_is_loaded;
    neuralNet *my_net;

    void createMenus();
    void createActions();
    void showFileOpenDialog();
    bool loadFile(const QString &fileName);
    bool saveFile();

signals:
    void saveNet();
    void close();
};

#endif // MAINWINDOW_H
