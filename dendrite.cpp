#include "dendrite.h"

dendrite::dendrite()
{
}
dendrite::~dendrite()
{

}
void dendrite::read(const QJsonObject &json)
{
    to = json["to"].toDouble();
    weight = json["wt"].toInt();
}
void dendrite::write(QJsonObject &json) const
{
    json["to"] = to;
    json["wt"] = weight;
}
