#ifndef FUNCTIONTESTERWIDGET_H
#define FUNCTIONTESTERWIDGET_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGLWidget>
#include <QPushButton>
#include <neuron.h>
#include "excitatory_synapse.h"
#include "inhibitory_synapse.h"
#include "random.h"
#include <QList>

namespace Ui {
class functionTesterWidget;
}

class functionTesterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit functionTesterWidget(QWidget *parent = 0);
    ~functionTesterWidget();


private:
    Ui::functionTesterWidget *ui;
    neuron *my_neuron;
    QHash<qint32, synapse *> *incoming_synapses;
    QGraphicsView *gW;
    QGraphicsScene *scene;
    QGraphicsItem *neuron_core;
    QLinearGradient gradient;
    QPen *my_pen;
    int neuron_rad;
    int synapse_rad;
    int dendrite_rad;


public slots:
    void setupScene();
    void testAnimation();

protected:

private slots:
    void on_buttonLoad_clicked();
};


#endif // FUNCTIONTESTERWIDGET_H
