#ifndef SRANNLOADEDWINDOW_H
#define SRANNLOADEDWINDOW_H

#include <QWidget>
#include "QThread"
#include "neuralnet.h"
#include "netreturnstruct.h"
#include "neuronmodel.h"

namespace Ui {
class SRANNLoadedWindow;
}

class SRANN : public QWidget
{
    Q_OBJECT

public:
    explicit SRANN(QWidget *parent = 0, neuralNet **net = NULL);
    ~SRANN();

private:
    Ui::SRANNLoadedWindow *ui;
    neuralNet *my_net;
    QThread *my_thread;
    neuronmodel *my_model;
    void setupNeuralNetAndUI();

private slots:
    void onNetHasCycled(net_return_data return_data);
    void onNetIsRunning();
    void onNetIsStopped();
    void on_stopButton_clicked();
    void on_resetButton_clicked();
    void on_runButton_clicked();
    void on_injectFireButton_clicked();
    void onNetSave();
    void saveNet();
    void checkInput();
    void onClose();

signals:
    void netIsSaved();
};

#endif // SRANNLOADEDWINDOW_H
