#ifndef DEFAULTWIDGET_H
#define DEFAULTWIDGET_H
//#include "buildnumber.h"
#include <QWidget>
#include <QDate>
#include <QTimer>


namespace Ui {
class defaultWidget;
}

class defaultWidget : public QWidget
{
    Q_OBJECT

public:
    explicit defaultWidget(QWidget *parent = 0);
    ~defaultWidget();

private:
    Ui::defaultWidget *ui;

private slots:
    void showcurrentTime();
};

#endif // DEFAULTWIDGET_H
