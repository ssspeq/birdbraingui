#include "defaultwidget.h"
#include "ui_defaultwidget.h"

defaultWidget::defaultWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::defaultWidget)
{
    ui->setupUi(this);
    showcurrentTime();
    QString compilationTime = QString("%1T%2").arg(__DATE__).arg(__TIME__);
    QString colour = "RED";
    QString fonttemplate = tr("<font color='%1'>%2</font>");
    ui->buildLabel->setText( fonttemplate.arg( colour, compilationTime ) );

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showcurrentTime()));
    timer->start(1000);

}

defaultWidget::~defaultWidget()
{
    delete ui;
}

void defaultWidget::showcurrentTime()
{
    QTime times = (QTime::currentTime());
    QString currentTime=times.toString("hh:mm:ss");
    ui->timeLabel->setText(currentTime);
}
