#ifndef DIALOGNETCREATE_H
#define DIALOGNETCREATE_H
#include "neuralnet.h"
#include <QString>
#include <QDialog>
#include <QPushButton>

namespace Ui {
class DialogNetCreate;
}

class DialogNetCreate : public QDialog
{
    Q_OBJECT

public:
    explicit DialogNetCreate(QWidget *parent = 0);
    neuralNet *my_net;
    neuralNet *getLoadedNet();
    ~DialogNetCreate();


private slots:
    void checkInput();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::DialogNetCreate *ui;
};

#endif // DIALOGNETCREATE_H
