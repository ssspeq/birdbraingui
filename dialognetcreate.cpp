#include "dialognetcreate.h"
#include "ui_dialognetcreate.h"


DialogNetCreate::DialogNetCreate(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogNetCreate)
{
    ui->setupUi(this);
    my_net = new neuralNet(0);
    ui->numberOfNeuronsLineEdit->setValidator( new QIntValidator(0, 100000, this) );
    ui->numberOfSynapsesLineEdit->setValidator( new QIntValidator(0, 20, this) );
    ui->comboBox->addItem("SRANN");
    ui->comboBox->addItem("FANN (NOT IMPLEMENTED YET)");
    ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( false );
    connect(ui->networkNameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(checkInput()));
    //connect(ui->networkTypeWidget, SIGNAL(textChanged()), this, SLOT(checkInput()));
    connect(ui->filenameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(checkInput()));
    connect(ui->numberOfNeuronsLineEdit, SIGNAL(textEdited(QString)), this, SLOT(checkInput()));
    connect(ui->numberOfSynapsesLineEdit, SIGNAL(textEdited(QString)), this, SLOT(checkInput()));

}

DialogNetCreate::~DialogNetCreate()
{
    delete ui;
}

void DialogNetCreate::checkInput()
{
    bool ok = !ui->networkNameLineEdit->text().isEmpty()
            && !ui->filenameLineEdit->text().isEmpty()
            && !ui->numberOfNeuronsLineEdit->text().isEmpty()
            && !ui->numberOfSynapsesLineEdit->text().isEmpty();
    ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( ok );
}


neuralNet *DialogNetCreate::getLoadedNet()
{
    return my_net;
}


void DialogNetCreate::on_buttonBox_accepted()
{
    my_net->name = (QString)ui->networkNameLineEdit->text();
    my_net->id = ui->networkIDSpinBox->text().toInt();
    this->setCursor(Qt::WaitCursor);
    my_net->populateRandom(ui->numberOfNeuronsLineEdit->text().toInt()
    , (qint32)ui->numberOfSynapsesLineEdit->text().toInt());
    my_net->filename = (QString)ui->filenameLineEdit->text();
    my_net->type = 0;
    this->setCursor(Qt::ArrowCursor);
    QDialog::accept();
}

void DialogNetCreate::on_buttonBox_rejected()
{
    this->close();
}
