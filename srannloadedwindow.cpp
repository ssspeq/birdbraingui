#include "srannloadedwindow.h"
#include "ui_srannloadedwindow.h"

SRANN::SRANN(QWidget *parent, neuralNet **net) :
    QWidget(parent),
    ui(new Ui::SRANNLoadedWindow)
{
    my_net = *net;
    ui->setupUi(this);
    setupNeuralNetAndUI();
    //Below is so the net can return custom data
    qRegisterMetaType<net_return_data>("net_return_data");
}

SRANN::~SRANN()
{
    delete ui;
}

void SRANN::saveNet()
{
    my_net->saveNet(my_net->Json, my_net->filename);
    emit netIsSaved();
}

void SRANN::onClose()
{
    qDebug() << "Got close signal";
    my_thread->quit();
    delete my_thread;
    delete my_net;
    this->deleteLater();
    this->close();
}

void SRANN::setupNeuralNetAndUI()
{
    ui->networkNameLineEdit->setText(my_net->name);
    ui->fileNameLineEdit->setText(my_net->filename);
    ui->potentialAfterFireDoubleSpinBox->setValue(my_net->charge_after_fire);
    ui->potentialAtRestDoubleSpinBox->setValue(my_net->resting_potential);
    ui->potentialGainDoubleSpinBox->setValue(my_net->charge_gain);
    ui->potentialLeakDoubleSpinBox->setValue(my_net->charge_leak);
    ui->potentialThresholdDoubleSpinBox->setValue(my_net->charge_threshold);

    ui->activationFunctionComboBox->addItem("Level");
    ui->stopButton->setDisabled(true);
    my_model = new neuronmodel(this, &my_net);
    ui->netTableView->setModel(my_model);
    my_thread = new QThread(this);
    //thread->deleteLater();
    my_net->moveToThread(my_thread);

    connect( my_thread, SIGNAL(started()), my_net, SLOT(cycleNetRunFromThread()));

    connect( my_net, SIGNAL(stopThread()), my_thread, SLOT(quit()));

    connect(my_net, SIGNAL(netHasCycled(net_return_data)),
            SLOT(onNetHasCycled(net_return_data)));

    connect(my_net, SIGNAL(netIsRunning()),SLOT(onNetIsRunning()));

    connect(my_net, SIGNAL(netIsStopped()),SLOT(onNetIsStopped()));

    connect(ui->fileNameLineEdit, SIGNAL(textEdited(QString)),
            this, SLOT(checkInput()));

    connect(ui->networkNameLineEdit, SIGNAL(textEdited(QString)),
            this, SLOT(checkInput()));

    connect(ui->potentialAfterFireDoubleSpinBox,SIGNAL(valueChanged(double)),
            this,SLOT(checkInput()));

    connect(ui->potentialAtRestDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(checkInput()));

    connect(ui->potentialGainDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(checkInput()));

    connect(ui->potentialLeakDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(checkInput()));

    connect(ui->potentialThresholdDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(checkInput()));
}

void SRANN::checkInput()
{
    qDebug() <<"value changed spinbox";
    my_net->filename = ui->fileNameLineEdit->text();
    my_net->name = ui->networkNameLineEdit->text();
    my_net->charge_after_fire = ui->potentialAfterFireDoubleSpinBox->value();
    my_net->resting_potential = ui->potentialAtRestDoubleSpinBox->value();
    my_net->charge_gain = ui->potentialGainDoubleSpinBox->value();
    my_net->charge_leak = ui->potentialLeakDoubleSpinBox->value();
    my_net->charge_threshold = ui->potentialThresholdDoubleSpinBox->value();

}

void SRANN::onNetHasCycled(net_return_data return_data)
{
    ui->neuronsFiringLabelActual->setText(QString::number(return_data.neurons_firing));
    ui->cycleCountActual->setText(QString::number(return_data.cycle_count));
}

void SRANN::onNetIsRunning()
{
    ui->runButton->setDisabled(true);
    ui->stopButton->setEnabled(true);
    ui->resetButton->setDisabled(true);

}

void SRANN::onNetIsStopped()
{
    ui->runButton->setEnabled(true);
    ui->stopButton->setDisabled(true);
    ui->resetButton->setEnabled(true);
}

void SRANN::on_stopButton_clicked()
{
    my_net->stopNet();
}


void SRANN::on_resetButton_clicked()
{
    my_net->reset();
    ui->cycleCountActual->setText("0");
}

void SRANN::on_runButton_clicked()
{

    qint32 cycles = ui->cyclesToRunEdit->text().toInt();
    my_net->cycles_to_run = cycles;
    my_thread->start();
}


void SRANN::on_injectFireButton_clicked()
{

}

void SRANN::onNetSave()
{
    saveNet();
}
