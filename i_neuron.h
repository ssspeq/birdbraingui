#ifndef I_NEURON_H
#define I_NEURON_H
#include "neuron.h"

class i_neuron : public neuron
{
public:
    void fire();
    i_neuron();
    ~i_neuron();
};

#endif // I_NEURON_H
