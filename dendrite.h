#ifndef DENDRITE_H
#define DENDRITE_H
#include "synapse.h"

class dendrite
{
public:
    synapse *my_synapse = NULL;
    float weight;
    qint32 to;
    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
    dendrite();
    ~dendrite();
};

#endif // DENDRITE_H
