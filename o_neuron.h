#ifndef O_NEURON_H
#define O_NEURON_H
#include "neuron.h"
class o_neuron : public neuron
{
public:
    void fire();
    o_neuron();
    ~o_neuron();
};

#endif // O_NEURON_H
