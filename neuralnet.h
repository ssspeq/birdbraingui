#ifndef NEURALNET_H
#define NEURALNET_H
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include "device.h"
#include "neuron.h"
#include "i_neuron.h"
#include "p_neuron.h"
#include "o_neuron.h"
#include "e_neuron.h"
#include "constants.h"
#include "synapse.h"
#include <qfile.h>
#include <QDebug>
#include "netreturnstruct.h"
#include <QObject>

class neuralNet : public QObject
{
    Q_OBJECT
public:
    explicit neuralNet(QObject *parent = 0);

    enum SaveFormat {
            Json, Binary
        };

    qint32 id;
    QString name;
    QString filename;
    int type;
    qint64 cycle_count;
    QHash<qint32, neuron *> *node_hash;
    QHash<qint32, device *> *device_hash;
    qint32 cycles_to_run;
    volatile bool keep_running;
    //Default values for this net, neurons can override
    qint8 activation_function;
    qint8 neuron_type;
    float charge_threshold;
    float charge_gain;
    float charge_leak;
    float charge_after_fire;
    float charge_threshold_self_fire;
    float resting_potential;

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
    bool loadNet(SaveFormat saveFormat, QString filename);
    bool saveNet(SaveFormat saveFormat, QString filename) const;
    void populateRandom(qint32 num_neurons, qint32 num_synapses_per_neuron);
    void synapsesAddRandom(neuron **node,qint32 num_nodes, qint32 num_synapses);
    void addSynapse(synapse **synapse, neuron **node, qint32 num_neurons);
    void updateDendrites();
    net_return_data cycleNet(int cycles, bool print);
    net_return_data cycleNetOnceInTimeSteps(bool print);
    void injectNeuronFire(qint32 id);
    void reset();
    neuralNet();
    ~neuralNet();
signals:
    void netIsRunning();
    void netIsStopped();
    void netHasCycled(net_return_data);
    void stopThread();
public slots:
    void cycleNetRunFromThread();
    void stopNet();
};

#endif // NEURALNET_H
