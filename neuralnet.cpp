#include "neuralnet.h"

neuralNet::neuralNet(QObject *parent) :
    QObject(parent)
{
    node_hash = new QHash<qint32, neuron *>;
    device_hash = new QHash<qint32,device *>;

    cycle_count = 0;
    cycles_to_run = 0;
    keep_running = false;

    activation_function = ACTIVATION_FUNCTION_LEVEL;
    charge_threshold = 0.7;
    charge_after_fire = -0.5;
    charge_gain = 0;
    charge_leak = -0.1;
    charge_threshold_self_fire = 0;
    resting_potential = 0;
}
neuralNet::~neuralNet()
{
    if(node_hash->isEmpty() != true)
    {
        QHash<qint32,neuron *>::iterator i;
        for (i = node_hash->begin(); i != node_hash->end(); i++)
        {
            delete &*i.value();
        }
        node_hash->clear();

    }
    delete node_hash;

    if(device_hash->isEmpty() != true)
    {
        QHash<qint32,device *>::iterator i;
        for (i = device_hash->begin(); i != device_hash->end(); i++)
        {
            delete &*i.value();
        }
        device_hash->clear();

    }
    delete device_hash;
    qDebug() << "delete";

}

bool neuralNet::loadNet(neuralNet::SaveFormat saveFormat, QString filename)
{
    QFile loadFile(saveFormat == Json
                   ? QString(filename+".json")
                   : QString(filename+".dat"));
    loadFile.setFileName(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }
    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(saveFormat == Json
                          ? QJsonDocument::fromJson(saveData)
                          : QJsonDocument::fromBinaryData(saveData));
    read(loadDoc.object());

    return true;
}

bool neuralNet::saveNet(neuralNet::SaveFormat saveFormat, QString filename) const
{
    QFile saveFile(saveFormat == Json
                   ? QString(filename+".json")
                   : QString(filename+".dat"));
    saveFile.setFileName(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QJsonObject netObject;
    write(netObject);

    QJsonDocument saveDoc(netObject);
    saveFile.write(saveFormat == Json
                   ? saveDoc.toJson()
                   : saveDoc.toBinaryData());

    return true;
}

void neuralNet::read(const QJsonObject &json)
{

    id = json["id"].toDouble();
    name = json["name"].toString();
    filename = json["fname"].toString();
    type = json["te"].toInt();
    activation_function = json["af"].toInt();
    charge_threshold = json["ct"].toDouble();
    charge_gain = json["cg"].toDouble();
    charge_leak = json["cl"].toDouble();
    resting_potential = json["rp"].toDouble();
    charge_after_fire = json["caf"].toDouble();

    node_hash->clear();

    QJsonArray node_array = json["nodes"].toArray();
    for (int node_index = 0; node_index < node_array.size(); ++node_index)
    {
        QJsonObject neuron_object = node_array[node_index].toObject();
        neuron *node = NULL;
        switch(neuron_object["nt"].toInt())
        {
        case NEURON_TYPE_EXCITATORY:
            node = new e_neuron;
            break;
        case NEURON_TYPE_INHIBITORY:
            node = new i_neuron;
            break;
        case NEURON_TYPE_PACEMAKER:
            node = new p_neuron;
            break;
        }
        node->read(neuron_object);
        node_hash->insert(node->id, node);
    }
    updateDendrites();

}
void neuralNet::write(QJsonObject &json) const
{
    json["id"] = id;
    json["name"] = name;
    json["fname"] = filename;
    json["te"] = type;
    json["af"] = activation_function;
    json["ct"] = charge_threshold;
    json["cg"] = charge_gain;
    json["cl"] = charge_leak;
    json["rp"] = resting_potential;
    json["caf"] = charge_after_fire;
    QJsonArray node_array;

    QHash<qint32, neuron *>::iterator i;
    for (i = node_hash->begin(); i != node_hash->end(); ++i)
    {
        QJsonObject json_node;
        (*i)->write(json_node);
        node_array.append(json_node);
    }
    json["nodes"] = node_array;
}

void neuralNet::populateRandom(qint32 num_neurons, qint32 num_synapses_per_neuron)
{
    //Make random neurons
    neuron *new_neuron;
    int random_neuron_type = 0;
    qint32 i;
    for(i=0; i< num_neurons; i++)
    {
        new_neuron = NULL;
        random_neuron_type = RandLim(100);
        if(random_neuron_type <= 5)
        {
            new_neuron = new p_neuron;
            new_neuron->neuron_type = NEURON_TYPE_PACEMAKER;
        }
        else if(random_neuron_type > 5 && random_neuron_type <= 52)
        {
            new_neuron = new e_neuron;
            new_neuron->neuron_type = NEURON_TYPE_EXCITATORY;
        }
        else if(random_neuron_type > 52 && random_neuron_type <= 100)
        {
            new_neuron = new i_neuron;
            new_neuron->neuron_type = NEURON_TYPE_INHIBITORY;
        }
        new_neuron->id = i;
        node_hash->insert(new_neuron->id,new_neuron);
    }
    //Now add synapses
    QHash<qint32,neuron *>::iterator y;
    for(y = node_hash->begin(); y != node_hash->end(); ++y)
    {
        synapsesAddRandom(&(*y), num_neurons, num_synapses_per_neuron);
    }

    //Now dendrites
    //TODO: FIX ALL THIS CRAP ITS A MESS!
    for(y = node_hash->begin(); y != node_hash->end(); ++y)
    {
        QLinkedList<synapse *>::iterator z;
        for(z = y.value()->synapse_list->begin();
            z != y.value()->synapse_list->end(); ++z)
        {
            neuron *target_node;
            qint32 id = (*z)->to;
            target_node = node_hash->find(id).value();
            dendrite *new_dendrite = new dendrite;
            new_dendrite->to = y.value()->id;
            new_dendrite->my_synapse = (*z);
            new_dendrite->weight = RandFloatMinusOneToOne();
            target_node->dendrite_list->append(new_dendrite);

        }
    }
}

void neuralNet::updateDendrites()
{
    //Now dendrites
    QHash<qint32,neuron *>::iterator y;
    for(y = node_hash->begin(); y != node_hash->end(); ++y)
    {
        QLinkedList<synapse *>::iterator z;
        for(z = y.value()->synapse_list->begin();
            z != y.value()->synapse_list->end(); ++z)
        {
            neuron *target_node;
            qint32 id = (*z)->to;
            target_node = node_hash->find(id).value();

            QLinkedList<dendrite *>::iterator it;
            for(it = target_node->dendrite_list->begin();
                it != target_node->dendrite_list->end(); ++it)
            {
                //qDebug() << "test";
                if((*it)->to == y.value()->id)
                {
                    (*it)->my_synapse = (*z);
                }
            }
        }

    }
}

void neuralNet::synapsesAddRandom(neuron **node,qint32 num_nodes, qint32 num_synapses)
{
    neuron *current_node = *node;
    synapse *new_synapse = NULL;
    qint32 i;
    for(i=0;i<num_synapses;i++)
    {
        //Check neuron type to see what kind of synapses it should have

        if(current_node->neuron_type == NEURON_TYPE_EXCITATORY)
        {
            if(RandLim(50) < 2)
            {
                new_synapse = new electrical_synapse;
                new_synapse->type = ELECTRICAL;
                addSynapse(&new_synapse, &current_node, num_nodes);
            }
            else
            {
                new_synapse = new excitatory_synapse;
                new_synapse->type = CHEMICAL_EXC;
                addSynapse(&new_synapse, &current_node, num_nodes);
            }
        }
        else if(current_node->neuron_type == NEURON_TYPE_INHIBITORY)
        {
            if(RandLim(10) < 3)
            {
                new_synapse = new electrical_synapse;
                new_synapse->type = ELECTRICAL;
                addSynapse(&new_synapse, &current_node, num_nodes);
            }
            else
            {
                new_synapse = new inhibitory_synapse;
                new_synapse->type = CHEMICAL_INHIB;
                addSynapse(&new_synapse, &current_node, num_nodes);
            }
        }
        else if(current_node->neuron_type == NEURON_TYPE_PACEMAKER)
        {
            new_synapse = new inhibitory_synapse;
            new_synapse->type = CHEMICAL_INHIB;
            addSynapse(&new_synapse, &current_node, num_nodes);

        }
    }
}

void neuralNet::addSynapse(synapse **Synapse, neuron **node, qint32 num_neurons)
{
    synapse *new_synapse = *Synapse;
    neuron *current_node = *node;
    qint32 target_id = 0;
    target_id = RandLim(num_neurons-1);
    new_synapse->to = target_id;
    new_synapse->from = current_node->id;
    new_synapse->weight = RandFloatMinusOneToOne();
    new_synapse->usage_level = 0;
    current_node->synapse_list->append(new_synapse);
}
net_return_data neuralNet::cycleNet(int cycles, bool print)
{
    int firing_neurons;
    int y;
    for(y=0; y<cycles;y++)
    {
        firing_neurons = 0;
        QHash<qint32,neuron *>::iterator i;
        for(i=node_hash->begin();i!=node_hash->end();i++)
        {
            firing_neurons += (i.value())->doCompleteTimeStep();
        }
        if(print)
        {
            qDebug() << "Cycle:" << y << "Firing neurons:" << firing_neurons;
        }
        cycle_count++;
    }
    net_return_data my_return_data;
    my_return_data.neurons_firing = firing_neurons;
    my_return_data.cycle_count = cycle_count;
    return my_return_data;
}
net_return_data neuralNet::cycleNetOnceInTimeSteps(bool print)
{
    int firing_neurons = 0;
    QHash<qint32,neuron *>::iterator i;
    for(i=node_hash->begin();i!=node_hash->end();i++)
    {
        i.value()->checkDendrites();
    }
    //Now all neurons have updated input, lets see it they fire
    for(i=node_hash->begin();i!=node_hash->end();i++)
    {
        if(i.value()->activationFunctionLevel())
        {
            i.value()->fire();
            firing_neurons++;
        }
        else
        {
            i.value()->updateSynapses();
        }
    }



    if(print)
    {
        qDebug() << "Cycle:" << "0" << "Firing neurons:" << firing_neurons;
    }
    cycle_count++;
    net_return_data my_return_data;
    my_return_data.neurons_firing = firing_neurons;
    my_return_data.cycle_count = cycle_count;
    return my_return_data;

}

void neuralNet::reset()
{
    QHash<qint32,neuron *>::iterator i;
    for(i=node_hash->begin();i!=node_hash->end();i++)
    {
        i.value()->reset();
    }
    cycle_count = 0;
}

void neuralNet::injectNeuronFire(qint32 id)
{
    if(node_hash->contains(id))
    {
        neuron *target_neuron = node_hash->find(id).value();
        target_neuron->charge = 1;
    }
}

void neuralNet::cycleNetRunFromThread()
{
    keep_running = true;
    emit netIsRunning();
    while(keep_running && cycles_to_run > 0)
    {
        net_return_data return_data = cycleNetOnceInTimeSteps(false);
        cycles_to_run--;
        emit netHasCycled(return_data);
    }
    stopNet();
}

void neuralNet::stopNet()
{
    keep_running = false;
    emit netIsStopped();
    emit stopThread();
}
