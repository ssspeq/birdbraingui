#ifndef E_NEURON_H
#define E_NEURON_H
#include "neuron.h"
class e_neuron : public neuron
{
public:
    void fire();
    e_neuron();
    ~e_neuron();
};

#endif // E_NEURON_H
