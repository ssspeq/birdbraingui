#ifndef NETRETURNSTRUCT_H
#define NETRETURNSTRUCT_H
struct net_return_data
{
    qint32 neurons_firing;
    qint64 cycle_count;
};
typedef struct net_return_data net_return_data;
#endif // NETRETURNSTRUCT_H
