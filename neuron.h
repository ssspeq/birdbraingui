#ifndef NEURON_H
#define NEURON_H
#include "synapse.h"
#include <qcoreapplication.h>
#include <QLinkedList>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include "constants.h"
#include "electrical_synapse.h"
#include "inhibitory_synapse.h"
#include "excitatory_synapse.h"
#include "random.h"
#include "dendrite.h"

class neuron
{


public:

    QLinkedList<synapse *> *synapse_list;
    QLinkedList<dendrite *> *dendrite_list;
    qint32 id;
    float charge;
    qint8 activation_function;
    float x;
    float y;
    float z;
    qint8 neuron_type;
    float charge_threshold;
    float charge_gain;
    float charge_leak;
    float charge_after_fire;
    float charge_threshold_self_fire;
    float resting_potential;
    virtual void read(const QJsonObject &json);
    virtual void write(QJsonObject &json) const;
    bool activationFunctionLevel();
    virtual void fire();
    virtual int doCompleteTimeStep();
    virtual void reset();
    virtual void checkDendrites();
    virtual void updateSynapses();
    neuron();
    virtual ~neuron();
};

#endif // NEURON_H
