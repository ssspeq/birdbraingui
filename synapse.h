#ifndef CONNECTION_H
#define CONNECTION_H
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
class synapse
{

public:

    qint32 to;
    qint32 from;
    float weight;
    int type;
    bool activated;
    int usage_level;
    bool passed_on_to_forward_neuron;
    virtual void read(const QJsonObject &json);
    virtual void write(QJsonObject &json) const;
    virtual void activate();
    virtual void reset();
    virtual void update();
    virtual void inactivate();

    synapse();
    virtual ~synapse();

private:
    qint8 time_to_be_active;
    qint8 time_activated;
};

#endif // CONNECTION_H
