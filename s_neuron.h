#ifndef S_NEURON_H
#define S_NEURON_H
#include "neuron.h"

class s_neuron : public neuron
{
public:
    void fire();
    s_neuron();
    ~s_neuron();
};

#endif // S_NEURON_H
